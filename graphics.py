from constant import *
import os
# WINDOWS:
from colorama import just_fix_windows_console, Cursor
just_fix_windows_console()

def clear_screen():
    os.system('clear')

def hide_cursor():
    print("\x1b[?25l") # hidden

def show_cursor():
    print("\x1b[?25h") # shown

def move_cursor(x, y):
    print("\033[%d;%dH" % (y, x),end='')

def add_marker(marker, position_x, position_y):
        move_cursor(position_x, position_y)
        print(marker, end='')

def draw_line(marker, length, direction_x,direction_y, origin_x, origin_y):
    for i in range(length):
        move_cursor(origin_x+i*direction_x, origin_y+i*direction_y)
        print(marker,end='')

def draw_border(origin_x, origin_y ,width ,height):
    add_marker("\u250F", origin_x, origin_y)    
    add_marker("\u2513", origin_x+width+1, origin_y)    

    add_marker("\u2517", origin_x, origin_y+height+1)    
    add_marker("\u251B", origin_x+width+1, origin_y+height+1)    

    draw_line("\u2501",width,1,0,origin_x+1,origin_y)
    draw_line("\u2501",width,1,0,origin_x+1,origin_y+height+1)
    draw_line("\u2503",height,0,1,origin_x,origin_y+1)
    draw_line("\u2503",height,0,1,origin_x+width+1,origin_y+1)
    print("") 

def draw_game_state(T, origin_x, origin_y):
    for i in range(W):
        for j in range(H):
            marker="\u25A9"            
            if(T[i][j] == 0):
                marker = "\u2B1E"
            else:
                if(T[i][j]==1):
                    marker=bcolors.BLUE
                if(T[i][j]==2):
                    marker=bcolors.GREEN
                if(T[i][j]==3):
                    marker=bcolors.MAGENTA
                if(T[i][j]==4):
                    marker=bcolors.RED
                if(T[i][j]==5):
                    marker=bcolors.HEADER
                if(T[i][j]==6):
                    marker=bcolors.CYAN
                if(T[i][j]==7):
                    marker=bcolors.YELLOW
                marker = marker+"\u25AE"+bcolors.ENDC
            add_marker(marker, (origin_x+1) + i, (origin_y+1) + j)

def draw_game_state_debug(T, origin_x, origin_y):
    S="||DEBUG||"
    c=0
    for s in S:
        marker=s
        add_marker(marker, (origin_x + 1) + c, (origin_y)) 
        c=c+1
    for i in range(W):
        for j in range(H):
            marker=str(T[i][j]) 

            add_marker(marker, (origin_x+1) + i, (origin_y+1) + j)

def draw(T):
    print(bcolors.ENDC, end='')
    # TETRIS WINDOW
    draw_border(game_grid_origin_x, game_grid_origin_y,W,H) # tetris window
    # NEXT BLOCK WINDOW
    draw_border(game_grid_origin_x + W + B , game_grid_origin_y, 4, 4 ) # tetris window
    # MAIN BORDER
    draw_border(1,1,3*W + B ,H+2*B) # window borders   
    # GAME STATE
    draw_game_state(T, game_grid_origin_x, game_grid_origin_y) # draw T array inside tetris window
    # DEBUG
    draw_game_state_debug(T, game_grid_debug_origin_x,game_grid_debug_origin_y)

    # flush buffer
    print("")
    # set cursor for printing debug logs
    move_cursor(game_grid_debug_origin_x + W + B, game_grid_debug_origin_y + 0)
    print(bcolors.HEADER,end='') 

def reset_terminal():
    # set cursor position at lower right side of screen
    move_cursor(game_grid_origin_x + W + 2*B, game_grid_debug_origin_y + H + 2*B)
    # white color mode
    print(bcolors.ENDC, end='')
    show_cursor()



