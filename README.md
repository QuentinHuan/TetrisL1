# Projet L1 Maths 2023:

Programmer un jeu de Tetris dans la console.

## Etape 1:
 - creer 3 fichiers (logic.py, graphics.py, tetris.py)
 - boucle d'affichage
 - affichage de la fenetre principale, de jeu, du prochain bloc
 - affichage du plateau de jeu
 - quitter en appuyant sur ESC
 - affichage plus beau avec caractere UNICODE

## Etape 2:
 - animer le plateau de jeu
 - bouger un bloc 1x1 en horizontal avec le clavier
 - gravitée
 - empiler les blocs
 - ajouter des couleurs différentes pour chaque nouveaux blocs
 - afficher le prochain bloc dans la petite fenetre à droite

## Etape 3:
 - detruire les lignes
 - perdu si ça dépasse
 - augmenter la vitesse du jeu au fil de la partie

## Etape 4:
 - des vrais blocs (L, carré, z, reverse z, |)
 - Afficher un score
 - [...]



