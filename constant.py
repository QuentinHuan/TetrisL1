W = 10 # width of Tetris screen
H = 20 # height of Tetris screen
B = 4  # border size

type_number = 7

game_grid_origin_x = W
game_grid_origin_y = 5

game_grid_debug_origin_x = 4*W
game_grid_debug_origin_y = 5

tetromino_origin_x = 4
tetromino_origin_y = 0


draw_freq = 0.05
update_freq = 0.25

# ANSI colors
class bcolors:
    HEADER = '\033[95m'
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    YELLOW = '\033[33m'
    MAGENTA = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
