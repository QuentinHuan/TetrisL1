from constant import *
from logic import *
from getkey import getkey, keys
from graphics import * 
import time as time

T = [[0 for j in range(H)] for i in range(W)] # game state, list completion
tetromino = Tetromino(5,5,2)
T[tetromino.pos_x][tetromino.pos_y] = tetromino.type 


QUIT_FLAG = False
draw_timer = 0
update_timer = 0

def input(key):
    if(key == keys.ESC):
        global QUIT_FLAG
        QUIT_FLAG = True
    if(key == keys.A):
        move_tetromino_horizontal(T, tetromino, -1)
    if(key == keys.D):
        move_tetromino_horizontal(T, tetromino, 1)
    if(key == keys.S):
        move_tetromino_vertical(T, tetromino, 1)

hide_cursor()
clear_screen()

while QUIT_FLAG==False: #Breaks when ESC key is pressed
    # INPUT()
    key = getkey(blocking=False)
    input(key)

    # DRAW() every <draw_freq> second
    if(time.time()-draw_timer > draw_freq):
        draw_timer = time.time()
        draw(T)

    # UPDATE() every <update_freq> second
    if(time.time()-update_timer > update_freq):
        update_timer = time.time()
        update(T, tetromino)

reset_terminal()
