from constant import *

# class for the moving tetromino
class Tetromino:
    def __init__(self, pos_x, pos_y, type):
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.type = type

# fill T array with colors: test display
def fill_color_T(T, offset):
    for i in range(W):
        for j in range(H):
            T[i][j] = ((i+j+offset)%type_number) + 1

def is_free(T, pos_x, pos_y):
    if(T[pos_x][pos_y]!=0):
        return False
    else:
        return True


# Move tetromino left and right
def move_tetromino_horizontal(T, tetromino, direction):
    new_pos_x = tetromino.pos_x+direction
    pos_x = tetromino.pos_x
    pos_y = tetromino.pos_y
    if( 0<=new_pos_x<W ) :
        if(is_free(T, new_pos_x, pos_y)):
            T[pos_x][pos_y] = 0
            tetromino.pos_x = new_pos_x
            T[new_pos_x][pos_y] = tetromino.type
    else: # prevent from moving outside
        return

# Move tetromino up and down
def move_tetromino_vertical(T, tetromino, direction):
    new_pos_y = tetromino.pos_y+direction
    pos_x = tetromino.pos_x
    pos_y = tetromino.pos_y
    if( 0<=new_pos_y<H ):
        if(is_free(T, pos_x, new_pos_y)):
            T[pos_x][pos_y] = 0
            tetromino.pos_y = new_pos_y 
            T[pos_x][new_pos_y] = tetromino.type
        else:
            stack_tetromino(T, tetromino)
    else: # prevent from moving outside
        stack_tetromino(T, tetromino)

# fix tetromino in place
def stack_tetromino(T, tetromino):
    T[tetromino.pos_x][tetromino.pos_y] = tetromino.type
    print("stack !")
    # move to origin
    tetromino.pos_x = tetromino_origin_x 
    tetromino.pos_y = tetromino_origin_y
    tetromino.type = (tetromino.type+1)%type_number + 1




# update game state
def update(T, tetromino):
    # test display
    # fill_color_T(T, T[0][0])
    move_tetromino_vertical(T, tetromino, 1)
